package com.sda.chat.message;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class MessageService {
	
	@Autowired
	private MessageDao messageDao;
	
	public void create(Message message){
		messageDao.create(message);
	}

	public List<Message> getAllMessages() {
		return messageDao.getAll();
	}

	public List<Message> getAllMessagesForUser(String user, String myId) {
		return messageDao.getForUser(user, myId);
	}

	public BigInteger getUnreadForUser(String user) {
		return messageDao.getUnreadCount(user);
	}

	public int updateMessage(String user) {
		return messageDao.update(user);
	}
	
	
}
