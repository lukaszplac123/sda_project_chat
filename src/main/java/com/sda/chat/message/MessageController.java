package com.sda.chat.message;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityExistsException;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/message")
public class MessageController {

	@Autowired
	MessageService messageService;
	
	@Autowired
	MessageRemoteService messageRemoteService;
	
	@RequestMapping(method = RequestMethod.GET, value="/get")
	public List<Message> getMessages(){
		List<Message> messagesReceived = messageRemoteService.getAllMessages();
		messagesReceived.forEach((message) -> {
			try{
			message.setHasBeenRead(false);
			messageService.create(message);
			}
			catch (DataIntegrityViolationException ex){
				Logger logger = Logger.getLogger("org.hibernate");
				logger.warn("Message exists. Save operation ommited. Next entity will be processed");
			}
		});
		return messageService.getAllMessages();
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value="/get/{markedUser}/{myId}")
	public List<Message> getMessages(@PathVariable("markedUser") String user, @PathVariable("myId") String myId){
		List<Message> messages = getMessages();
		return messageService.getAllMessagesForUser(user, myId);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/getUnread/{userId}")
	public BigInteger getMessages(@PathVariable("userId") String user){
		return messageService.getUnreadForUser(user);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value="/post")
	public Message createMessage(@RequestBody Message message){
		message.setCreated(new Date());
		Message messsageReceived = messageRemoteService.sendMessage(message);
		messsageReceived.setHasBeenRead(true);
		messageService.create(messsageReceived);
		return message;
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/upd")
	public void updateMessage(@RequestBody String user){
		int rowsUpdated = messageService.updateMessage(user);
		Logger logger = Logger.getLogger("org.hibernate");
		logger.warn(rowsUpdated + " rows updated");
	}
	
	
}
