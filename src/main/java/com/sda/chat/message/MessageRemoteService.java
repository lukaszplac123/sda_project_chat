package com.sda.chat.message;

import java.net.URI;
import java.net.URL;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;

import com.sda.chat.AbstractRemoteService;

@Service
public class MessageRemoteService extends AbstractRemoteService{

	private static final String MESSAGE_RESOURCE_URL = "/api/message";
	private RestTemplate restTemplate = new RestTemplate();
	
	public Message sendMessage(Message message){
		HttpHeaders headers = getDefaultHeaders();
		URI url = prepareUrl(MESSAGE_RESOURCE_URL);
		RequestEntity<Message> request = new RequestEntity<Message>(message, headers, HttpMethod.POST, url);
		Message messageResponse = restTemplate.postForObject(url, request, Message.class);
		return messageResponse;
	}
	
	public List<Message> getAllMessages(){
		HttpHeaders headers = getDefaultHeaders();
		URI url = prepareUrl(MESSAGE_RESOURCE_URL);
		RequestEntity<Message> request = new RequestEntity<Message>(headers, HttpMethod.GET, url);
		ResponseEntity<List<Message>> response = restTemplate.exchange(request, new ParameterizedTypeReference<List<Message>>() {});
		return response.getBody();
		
	}
	
}
