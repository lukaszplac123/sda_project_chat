package com.sda.chat.message;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

@Repository
public class MessageDao {
	
	@PersistenceContext
	EntityManager em;

	public void create(Message message){
		em.persist(message);
	}

	@SuppressWarnings("unchecked")
	public List<Message> getAll() {
		return em.createNativeQuery("SELECT * FROM MESSAGE ORDER BY created DESC", Message.class).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Message> getForUser(String user, String myId) {
		Query query = em.createNativeQuery("SELECT * FROM MESSAGE WHERE (receiver_id = ? AND sender_id = ?) OR (receiver_id = ? AND sender_id = ?) ORDER BY created DESC", Message.class);
		return query.setParameter(1, user)
					.setParameter(2, myId)
					.setParameter(3, myId)
					.setParameter(4, user)
					.getResultList();
	}

	public BigInteger getUnreadCount(String user) {
		Query query = em.createNativeQuery("SELECT COUNT(*) FROM MESSAGE WHERE sender_id = ? AND has_been_read = ?");
		return (BigInteger) query.setParameter(1, user)
								 .setParameter(2, false)
								 .getSingleResult();
	}

	public int update(String user) {
		Query query = em.createNativeQuery("UPDATE MESSAGE SET has_been_read = ? WHERE sender_id = ?");
		return query.setParameter(1, true)
					.setParameter(2, user)
					.executeUpdate();
	}
}