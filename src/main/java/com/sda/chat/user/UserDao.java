package com.sda.chat.user;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;


@Repository
public class UserDao {
	
	@PersistenceContext
	EntityManager em;

	public void create(UserName user){
		em.persist(user);
	}

	@SuppressWarnings("unchecked")
	public List<UserName> getAll() {
		return em.createNativeQuery("SELECT * FROM USER_NAME", UserName.class).getResultList();
	}
}
