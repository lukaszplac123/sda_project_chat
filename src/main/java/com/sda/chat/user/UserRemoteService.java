package com.sda.chat.user;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.sda.chat.AbstractRemoteService;

@Service
public class UserRemoteService extends AbstractRemoteService{

	private static final String USER_RESOURCE_URL = "/api/user";
	private RestTemplate restTemplate = new RestTemplate();
	
	public List<UserName> getAllUsernames(){
		HttpHeaders headers = getDefaultHeaders();
		URI url = prepareUrl(USER_RESOURCE_URL);
		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, url);
		ResponseEntity<String[]> response =
				restTemplate.exchange(request, new ParameterizedTypeReference<String[]>() {});
		List<UserName> users = new ArrayList<>();
		for (String userName : response.getBody()){
			users.add(new UserName(userName));
		}
		return users;
	}
}
