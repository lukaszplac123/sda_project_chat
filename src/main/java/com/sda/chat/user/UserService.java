package com.sda.chat.user;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class UserService {
	
	@Autowired
	private UserDao userDao;

	
	public void create(UserName user){
		userDao.create(user);
	}
	
	public List<UserName> getAllUsers() {
		return userDao.getAll();
	}
	
	
}
