package com.sda.chat.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class UserName {
	
	@Id
	@Column(name="userName", unique = true)
	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public UserName(String name){
		userName = name;
	}
	
	public UserName(){
		
	}
}
