package com.sda.chat.user;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.validation.ConstraintViolationException;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	UserService userService;
	
	@Autowired
	private UserRemoteService userRemoteService;
	
	@Value("${auth.username}")
	String username;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getUsername(){
		return username;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/all")
	public List<UserName> getUsers(){
		List<UserName> users = userRemoteService.getAllUsernames();
		users.forEach((user) -> {
			try{
			userService.create(user);
			} catch (DataIntegrityViolationException ex){
				Logger logger = Logger.getLogger("org.hibernate");
				logger.warn("User exists. Save operation ommited. Next entity will be processed");
			}
		});
		return userService.getAllUsers();
	}
}
