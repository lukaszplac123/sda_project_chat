$.ajaxSetup({contentType : 'application/json'})

let myId;
let markedUser;
let usersList = [];
let loadAll = false;

$.get('/api/user', function(user){
	$('h3').append(user);
	myId = user;
	markedUser = user;
})

$.get('/api/user/all', function(users){
	for (var i = 0 ; i < users.length; i++){
		appendUsers(users[i]);
		usersList.push(users[i].userName);
	}
})

$(function(){
	$('#confirm').click(function(){
	createMessage();
		});
})



function doPoll(){
	if (markedUser === myId){
		askForAllUsers();
	}else{
		askForMarkedUserData(markedUser);
	}
	updateUnreadAmount(markedUser);
	updateUnreadDatabase(markedUser);
	setTimeout(doPoll, 2500);
}

function updateUnreadAmount(markedUser){
	for (user of usersList){
		if (user != markedUser){
			changeUnread(user);
		} else {
			setUnreadToZeroFor(user);
		}
	}
}

function updateUnreadDatabase(markedUser){
	$.post('api/message/upd', markedUser, function(data){});
}

function askForMarkedUserData(markedUser){
	$.get('/api/message/get/'+markedUser+"/"+myId, function(messages){
		clearMessageContent();
		var length=0;
		if ((loadAll == true) || (messages.length <= 10)) {
			length = messages.length;
		} else if (messages.length > 10){
			length = 10
		}
		for (var i = 0 ; i < length; i++){
			appendMessage(messages[i]);
		}
		if ((loadAll==false) && (length >= 10)){
			$('#messages').append('<li class="list-group-item" id="load_all">Load All</li')
			$('#load_all').on("click", loadAllMessages)
			} else if (length >= 10){
				$('#messages').append('<li class="list-group-item" id="hide">Hide</li')
				$('#hide').on("click", hide)
			}
	 })
}

function askForAllUsers(){
	$.get('/api/message/get', function(messages){
		clearMessageContent();
		var length=0;
		if ((loadAll == true) || (messages.length <= 10)) {
			length = messages.length;
		} else if (messages.length > 10){
			length = 10
		}
		for (var i = 0 ; i < length; i++){
			appendMessage(messages[i]);
		 }
		if ((loadAll==false) && (length >= 10)){
			$('#messages').append('<li class="list-group-item" id="load_all">Load All</li')
			$('#load_all').on("click", loadAllMessages)
			} else if (length >= 10){
				$('#messages').append('<li class="list-group-item" id="hide">Hide</li')
				$('#hide').on("click", hide)
			}
		})
}

function loadAllMessages(){
	loadAll = true;
	$('#load_all').remove();
}

function hide(){
	loadAll = false;
	$('#hide').remove();
}

function clearMessageContent(message){
	$('#messages').html("");
}

function appendMessage(message){
	var date = new Date(message.created);
	var messageStyle = "list-group-item-success";
	if (message.receiverId !== myId) messageStyle="list-group-item-info"
	$('#messages').append('<li class="list-group-item '+messageStyle+'"+"><span style="color:red">' 
							+ message.senderId + ">"+message.receiverId+" : </span>"
							+'<span style="color:black">'+message.body 
							+'</span><div style="color:red" class="text-right">'+date.toLocaleDateString()+":"+date.toLocaleTimeString()+'</div></li>')
}
function appendUsers(user){
	$('#users').append($('<li>', {class: 'list-group-item'})
			.append(user.userName)
			.append("<span class='badge'></span>")
			.css("background-color","#FFFFC2")
			.on("click", userClicked)
			.attr("id",user.userName));
}

function changeUnread(user){
	$.get('/api/message/getUnread/'+user, function(unreadVal){
		 var selector = '#' + user + " span";
		 if (unreadVal > 0) {
			 $(selector).html(unreadVal);
			 $(selector).css("opacity","1");
			 $(selector).css("background-color","#DC381F");
		     $(selector).animate({
					 opacity: 0
				 }, 1000)
			 }
			 
		 }
	);
}


function setUnreadToZeroFor(user){
	var selector = '#' + user + " span";
	//clearInterval(badgeAnimationFunctionHandler);
	$(selector).stop();
	$(selector).html('');
}

function createMessage(){
	var message = {
		senderId : myId,
		receiverId : $('#receiverId').val(),
		body : $('#body').val()
	};
	$.post('api/message/post', JSON.stringify(message), function(data){}, 'json');
}

function userClicked(){
    let clickedUser = $(event.target);
    $('#users li').css("background-color","#FFFFC2")
    clickedUser.css("background-color","#FFDB58")
    let userId = clickedUser.attr("id");
    if (userId === myId){
    $(currentUser).html("ALL MESSAGES");
    	markedUser = myId;
    	askForAllUsers();
    } else{
    $(currentUser).html("CONVERSATION WITH " + userId);
    $('#receiverId').val(userId);
    markedUser = userId;
    askForMarkedUserData(userId);
    }
}

$(function(){
	doPoll();
});

